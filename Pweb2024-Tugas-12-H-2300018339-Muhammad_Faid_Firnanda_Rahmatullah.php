<?php

function cetakNama($arrNama, $arrNim, $arrNilai) {
    foreach ($arrNama as $index => $nama) {
        echo "Nama: " . $nama . "<br>";
        echo "NIM: " . $arrNim[$index] . "<br>";

        $nilaiIndividu = $arrNilai[$index];

        if ($nilaiIndividu >= 80 && $nilaiIndividu <= 100) {
            echo "Nilai: $nilaiIndividu (A), Lulus.<br>";
        } elseif ($nilaiIndividu >= 65 && $nilaiIndividu < 80) {
            echo "Nilai: $nilaiIndividu (B), Lulus.<br>";
        } elseif ($nilaiIndividu >= 50 && $nilaiIndividu < 65) {
            echo "Nilai: $nilaiIndividu (C), Lulus.<br>";
        } elseif ($nilaiIndividu >= 25 && $nilaiIndividu < 50) {
            echo "Nilai: $nilaiIndividu (D), Tidak Lulus.<br>";
        } elseif ($nilaiIndividu >= 0 && $nilaiIndividu < 25) {
            echo "Nilai: $nilaiIndividu (E), Tidak Lulus.<br>";
        } else {
            echo "Nilai yang Anda masukkan: $nilaiIndividu<br>Tolong inputkan nilai dengan benar!<br>";
        }

        echo "<br>"; 
    }
}

// Untuk memanggil fungsi:
$arrNama = array("Messi", "Ronaldo", "Mbappe","Halland","Lebron");
$arrNim = array(330, 377, 310, 309, 023);
$arrNilai = array(91, 77, 54, 40, 23);

echo "<b>Program untuk Menctak Nilai Mahasiswa</b><br>";
cetakNama($arrNama, $arrNim, $arrNilai);

?>
